import Vuex from "vuex";
import user from "./modules/user";
import good from "./modules/good";
import shop from "./modules/shop";

const createStore = () => {
    return new Vuex.Store({
        modules: {
            user,
            good,
            shop
        },
        state: {},
        mutations: {},
        actions: {}
    });
};

export default createStore;
