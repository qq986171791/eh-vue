import * as type from "./type";
import * as api from "./api";

export default {
    namespaced: true,
    state: {
        shopLists: []
    },
    mutations: {
        setState(state, payload) {
            for (let key in payload) {
                state[key] = payload[key];
            }
        }
    },
    actions: {
        async [type.setState]({ state, commit }, params) {
            commit("setState", params);
        },
        async [type.selectShop]({ state, commit }, params) {
            try {
                commit("setState", {
                    loading: true
                });
                let {
                    data: { latitude, longitude }
                } = await api.findCityPos();
                let res = await api.selectShop({
                    ...params,
                    latitude,
                    longitude
                });
                commit("setState", {
                    loading: false
                });
                if (200 === res.code) {
                    commit("setState", {
                        shopLists: res.data
                    });
                }
                return res;
            } catch (e) {
                // debugger;
            }
        }
    }
};
