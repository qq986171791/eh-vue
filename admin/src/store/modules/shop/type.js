export const findShop = "findShop";
export const selectShop = "selectShop";
export const saveShop = "saveShop";
export const addShop = "addShop";
export const delShop = "delShop";
export const setState = "setState";
export const findCityPos = "findCityPos";
