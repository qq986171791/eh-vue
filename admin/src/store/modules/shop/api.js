import fetch from "@/config/fetch";

export const selectShop = params =>
    fetch("/shopping/selectShop", params).then(res => {
        return {
            code: 200,
            data: res.concat().map(d => {
                return {
                    name: d.name,
                    address: d.address,
                    description: d.description,
                    id: d.id,
                    phone: d.phone,
                    rating: d.rating,
                    recent_order_num: d.recent_order_num,
                    category: d.category,
                    image_path: d.image_path
                };
            }),
            msg: "success"
        };
    });
export const findCityPos = () =>
    fetch("/v1/cities", {
        type: "guess"
    }).then(res => {
        return {
            code: 200,
            msg: "success",
            data: res
        };
    });
