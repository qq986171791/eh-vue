import * as type from "./type";
import * as api from "./api";

export default {
    namespaced: true,
    state: {
        login: {},
        info: {}
    },
    mutations: {
        setState(state, payload) {
            for (let key in payload) {
                state[key] = payload[key];
            }
        }
    },
    actions: {
        async [type.setState]({ state, commit }, params) {
            commit("setState", params);
        },
        async [type.findUser]({ state, commit }, params) {
            try {
                commit("setState", {
                    loading: true
                });
                let res = await api.findUser(params);
                commit("setState", {
                    loading: false
                });
                if (200 === res.code) {
                    commit("setState", {
                        info: res.data
                    });
                }
                return res;
            } catch (e) {
                // debugger;
            }
        },
        async [type.login]({ state, commit }, params) {
            try {
                commit("setState", {
                    loading: true
                });
                let res = await api.login(params);
                commit("setState", {
                    loading: false
                });
                if (200 === res.code) {
                    commit("setState", {
                        login: res.data
                    });
                }
                return res;
            } catch (e) {
                // debugger;
            }
        }
    }
};
