export const findUser = 'findUser';
export const selectUser = 'selectUser';
export const saveUser = 'saveUser';
export const addUser = 'addUser';
export const delUser = 'delUser';
export const setState = 'setState';
export const login = 'login';
