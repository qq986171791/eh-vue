export const findCategory = "findCategory";
export const selectCategory = "selectCategory";
export const saveCategory = "saveCategory";
export const addCategory = "addCategory";
export const delCategory = "delCategory";
export const setState = "setState";
