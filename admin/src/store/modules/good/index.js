import * as type from "./type";
import * as api from "./api";

export default {
    namespaced: true,
    state: {
        categoryLists: []
    },
    mutations: {
        setState(state, payload) {
            for (let key in payload) {
                state[key] = payload[key];
            }
        }
    },
    actions: {
        async [type.setState]({ state, commit }, params) {
            commit("setState", params);
        },
        async [type.addCategory]({ state, commit }, params) {
            try {
                commit("setState", {
                    loading: true
                });
                let res = await api.addCategory(params);
                commit("setState", {
                    loading: false
                });
                return res;
            } catch (e) {
                // debugger;
            }
        }
    }
};
