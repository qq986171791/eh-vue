import fetch from "@/config/fetch";

export const addCategory = params =>
    fetch("/shopping/addcategory", params, "POST");

export const selectCategory = params => fetch("/admin/info");
