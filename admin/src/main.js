import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store/";
import ElementUI from "element-ui";
import Vuex from "vuex";

import VueMaterial from "vue-material";

import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css"; // This line here

import Vuelidate from "vuelidate";

import VueQuillEditor from "vue-quill-editor";

// require styles
import "quill/dist/quill.core.css";
import "quill/dist/quill.snow.css";
import "quill/dist/quill.bubble.css";
import Paginate from "vuejs-paginate";
Vue.component("paginate", Paginate);

Vue.use(VueQuillEditor /* { default global options } */);

Vue.use(Vuelidate);

Vue.use(Vuex);

Vue.config.productionTip = false;

Vue.use(VueMaterial);

Vue.use(ElementUI);

new Vue({
    el: "#app",
    router,
    store,
    template: "<App/>",
    components: { App }
});
