import { baseUrl } from './env';

export const fetchUpload = (url, file) => {
    return fetch(url, {
        // Your POST endpoint
        method: 'POST',
        headers: undefined,
        body: file, // This is your file object
    });
};

export default async (
    url = '',
    data = {},
    type = 'GET',
    method = 'fetch',
    opts = {
        hasFile: false,
    },
) => {
    type = type.toUpperCase();
    url = baseUrl + url;

    let requestConfig = {
        credentials: 'include',
        method: type,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        mode: 'cors',
        cache: 'force-cache',
    };

    if (opts.hasFile) {
        requestConfig.headers = undefined;
        requestConfig.body = data;
    } else if (type === 'POST') {
        requestConfig.body = JSON.stringify(data);
    }

    if (type == 'GET') {
        let dataStr = ''; //数据拼接字符串
        Object.keys(data).forEach(key => {
            dataStr += key + '=' + data[key] + '&';
        });

        if (dataStr !== '') {
            dataStr = dataStr.substr(0, dataStr.lastIndexOf('&'));
            url = url + '?' + dataStr;
        }
    }

    if (window.fetch && method == 'fetch') {
        try {
            const response = await fetch(url, requestConfig);
            const responseJson = await response.json();
            return responseJson;
        } catch (error) {
            throw new Error(error);
        }
    } else {
        return new Promise((resolve, reject) => {
            let requestObj;
            if (window.XMLHttpRequest) {
                requestObj = new XMLHttpRequest();
            } else {
                requestObj = new ActiveXObject();
            }

            let sendData = '';
            if (type == 'POST') {
                sendData = JSON.stringify(data);
            }

            requestObj.open(type, url, true);
            requestObj.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            requestObj.send(sendData);

            requestObj.onreadystatechange = () => {
                if (requestObj.readyState == 4) {
                    if (requestObj.status == 200) {
                        let obj = requestObj.response;
                        if (typeof obj !== 'object') {
                            obj = JSON.parse(obj);
                        }
                        resolve(obj);
                    } else {
                        reject(requestObj);
                    }
                }
            };
        });
    }
};
