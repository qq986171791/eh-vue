'use strict';

module.exports = {
  port: 8001,
  url: 'mongodb://182.254.243.206:27017/faya',
  session: {
    name: 'SID',
    secret: 'SID',
    cookie: {
      httpOnly: true,
      secure: false,
      maxAge: 365 * 24 * 60 * 60 * 1000,
    },
  },
  domainC: 'http://api.zqdvue.com',
};
