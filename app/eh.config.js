module.exports = {
  local: {
    name: "ehNginx", //只能又一个容器映射80端口
    host: {
      www: "www.zqdvue.com",
      admin: "admin.zqdvue.com",
      api: "api.zqdvue.com"
    },
    port: {
      www: 8889,
      admin: 8888,
      api: 8001
    }
  }
};
